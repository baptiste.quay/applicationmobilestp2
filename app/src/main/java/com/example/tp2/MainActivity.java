package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    final WineDbHelper wineDbHelper = new WineDbHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //wineDbHelper.resetTable();

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Ajout impossible");
        builder.setMessage("Un vin portant le même nom existe déjà dans la base de données.");

        Bundle extras = getIntent().getExtras();
        if (extras!=null) {
            final Wine wine = extras.getParcelable("wine");
            try{
                wineDbHelper.updateWine(wine);
            }
            catch(Exception e){
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        }

        Cursor result = wineDbHelper.fetchAllWines();
        final SimpleCursorAdapter adapter =
                new SimpleCursorAdapter(this,
                        R.layout.list_item,result, new String[]{
                        WineDbHelper.COLUMN_NAME,
                        WineDbHelper.COLUMN_WINE_REGION},
                        new int[] { R.id.title, R.id.region}, 0);
        ListView lv = (ListView) findViewById(R.id.lstView);
        lv.setAdapter(adapter);

        registerForContextMenu(lv);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                Wine newWine = new Wine("nouveau vin", "", "", "", "");
                wineDbHelper.addWine(newWine);
                recreate();
            }
        });


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView parent, View v, int position, long id) {


                    final Cursor wineCursor = (Cursor) parent.getItemAtPosition(position);

                    Wine wine = wineDbHelper.cursorToWine(wineCursor);
                    Intent intent = new Intent(MainActivity.this, Wine_Activity.class);

                    intent.putExtra("wine", wine);
                    startActivity(intent);
                }



        });

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        if (v.getId() == R.id.lstView){
            AdapterView.AdapterContextMenuInfo info =(AdapterView.AdapterContextMenuInfo)menuInfo;
            MenuItem menu1=menu.add("Supprimer");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem menuItem) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuItem.getMenuInfo();
        int position = info.position;
        switch (menuItem.getItemId()) {
            case 0:
                Cursor cursor = wineDbHelper.fetchAllWines();
                cursor.moveToPosition(position);
                wineDbHelper.deleteWine(cursor);
                recreate();
                break;

            default:
                break;

        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
