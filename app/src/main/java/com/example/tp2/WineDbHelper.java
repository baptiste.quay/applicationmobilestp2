package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wineStorage";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE cellar(name TEXT, region TEXT,localization TEXT, climate TEXT, publisher TEXT, UNIQUE (name , region)ON CONFLICT ROLLBACK);");
        populate();

        // db.execSQL() with the CREATE TABLE ... command
    }

    public void resetTable(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("drop table if exists " + TABLE_NAME);
        db.execSQL(("CREATE TABLE cellar(name TEXT, region TEXT,localization TEXT, climate TEXT, publisher TEXT, UNIQUE (name , region)ON CONFLICT ROLLBACK);"));
        this.populate();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_NAME);
        onCreate(db);
    }


    /**
     * Adds a new wine
     *
     * @return true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        // Inserting Row
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        long rowID = db.insert(TABLE_NAME, COLUMN_NAME, cv);
        // call db.insert()
        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     *
     * @return the number of updated rows
     */
    public int updateWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_NAME, wine.getTitle());
        cv.put(COLUMN_WINE_REGION, wine.getRegion());
        cv.put(COLUMN_LOC, wine.getLocalization());
        cv.put(COLUMN_CLIMATE, wine.getClimate());
        cv.put(COLUMN_PLANTED_AREA, wine.getPlantedArea());
        int res = db.update(TABLE_NAME,cv,"rowid=?",new String[]{String.valueOf(wine.getId())} );
        return res;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(WineDbHelper.TABLE_NAME,
                new String[]{"ROWID AS " + _ID, COLUMN_NAME, COLUMN_WINE_REGION, COLUMN_LOC, COLUMN_CLIMATE, COLUMN_PLANTED_AREA},
                null, null, null, null, null);
        return cursor;

    }

    public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

        Wine wine = cursorToWine(cursor);

        db.delete(TABLE_NAME,"rowid=?",new String[]{String.valueOf(wine.getId())} );

    }

    public void populate() {
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));


        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM " + TABLE_NAME, null);
        Log.d(TAG, "nb of rows=" + numRows);
    }


    public static Wine cursorToWine(Cursor cursor) {
        int cpt = 0;
        try {
            int idIndex = cursor.getColumnIndexOrThrow(_ID);
            long id = cursor.getLong(idIndex);

            int idTitle = cursor.getColumnIndexOrThrow(COLUMN_NAME);
            String title = cursor.getString(idTitle);

            int idRegion = cursor.getColumnIndexOrThrow(COLUMN_WINE_REGION);
            String region = cursor.getString(idRegion);

            int idLocalization = cursor.getColumnIndexOrThrow(COLUMN_LOC);
            String localization = cursor.getString(idLocalization);

            int idClimate = cursor.getColumnIndexOrThrow(COLUMN_CLIMATE);
            String climate = cursor.getString(idClimate);

            int idPlantedArea = cursor.getColumnIndexOrThrow(COLUMN_PLANTED_AREA);
            String plantedArea = cursor.getString(idPlantedArea);
            return new Wine(id, title, region, localization, climate, plantedArea);


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
