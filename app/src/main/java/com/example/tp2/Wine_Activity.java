package com.example.tp2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Wine_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sauvegarde impossible");
        builder.setMessage("Le nom du vin ne doit pas être vide.");

        Bundle extras = getIntent().getExtras();

        final Wine wine = extras.getParcelable("wine");

        final EditText wineName = (EditText) findViewById(R.id.editName);
        wineName.setText(wine.getTitle());

        final EditText wineRegion = (EditText) findViewById(R.id.editRegion);
        wineRegion.setText(wine.getRegion());

        final EditText wineLocalization = (EditText) findViewById(R.id.editLoc);
        wineLocalization.setText(wine.getLocalization());

        final EditText wineClimate = (EditText) findViewById(R.id.editClimate);
        wineClimate.setText(wine.getClimate());

        final EditText winePlantedArea = (EditText) findViewById(R.id.editPlantedArea);
        winePlantedArea.setText(wine.getPlantedArea());

        final Button button = findViewById(R.id.save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                if(!wineName.getText().toString().matches("")){
                    wine.setTitle(wineName.getText().toString());
                    wine.setRegion(wineRegion.getText().toString());
                    wine.setLocalization(wineLocalization.getText().toString());
                    wine.setClimate(wineClimate.getText().toString());
                    wine.setPlantedArea(winePlantedArea.getText().toString());

                    Intent intent = new Intent(Wine_Activity.this, MainActivity.class);
                    intent.putExtra("wine", wine);
                    finish();
                    startActivity(intent);
                }
                else{
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                }


            }
        });

    }
}
